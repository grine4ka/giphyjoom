package ru.bykov.giphyjoom.ui

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.item_giphy_image.view.*

/**
 * @author Grigorii Bykov
 */
abstract class GiphyViewHolder<in T : GiphyDisplaybleItem>(view: View) : RecyclerView.ViewHolder(view) {
    open fun bind(item: T) = Unit

    open fun recycle() = Unit
}

class OffsetLoadingViewHolder(itemView: View) : GiphyViewHolder<OffsetLoadingItem>(itemView)

class ErrorLoadingViewHolder(
    itemView: View,
    retryClickListener: () -> Unit
) : GiphyViewHolder<ErrorLoadingItem>(itemView) {
    init {
        itemView.setOnClickListener {
            retryClickListener()
        }
    }
}

class GiphyImageViewHolder(
    itemView: View,
    private val requestOptions: RequestOptions,
    clickListener: (Int) -> Unit
) : GiphyViewHolder<GiphyImageItem>(itemView) {

    private val image: ImageView = itemView.giphyImage

    init {
        itemView.setOnClickListener {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                clickListener(position)
            }
        }
    }

    override fun bind(item: GiphyImageItem) {
        Glide.with(itemView.context)
            .load(item.giphyImage.url)
            .apply(requestOptions)
            .into(image)
    }

    override fun recycle() {
        Glide.with(itemView.context).clear(image)
    }
}
