package ru.bykov.giphyjoom.ui

import ru.bykov.giphyjoom.model.GiphyImage

/**
 * @author Grigorii Bykov
 */
interface OnImageClickListener {

    fun onImageClicked(image: GiphyImage)
}

interface OnRetryClickListener {

    fun onRetryClicked(offset: Int)
}