package ru.bykov.giphyjoom.ui

import ru.bykov.giphyjoom.R
import ru.bykov.giphyjoom.model.GiphyImage

/**
 * @author Grigorii Bykov
 */
interface GiphyDisplaybleItem {
    val viewType: Int
}

object ErrorLoadingItem : GiphyDisplaybleItem {
    override val viewType: Int = R.layout.item_offset_error
}

object OffsetLoadingItem : GiphyDisplaybleItem {
    override val viewType: Int = R.layout.item_offset_loading
}

class GiphyImageItem(val giphyImage: GiphyImage) : GiphyDisplaybleItem {
    override val viewType: Int = R.layout.item_giphy_image
}