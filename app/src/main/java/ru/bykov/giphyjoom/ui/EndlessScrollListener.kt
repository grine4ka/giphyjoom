package ru.bykov.giphyjoom.ui

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

const val DEFAULT_PAGE_SIZE = 50
const val DEFAULT_THRESHOLD = 8

@JvmOverloads
fun RecyclerView.offsetListener(
        pageSize: Int = DEFAULT_PAGE_SIZE,
        visibleThreshold: Int = DEFAULT_THRESHOLD,
        offsetListener: (Int) -> Unit
) {
    EndlessScrollListener(this, pageSize, visibleThreshold, offsetListener)
}

private class EndlessScrollListener(
    recyclerView: RecyclerView,
    private val pageSize: Int,
    private val visibleThreshold: Int,
    private val offsetListener: (Int) -> Unit
) {
    private val layoutManager = recyclerView.layoutManager as LinearLayoutManager
    private val adapter = recyclerView.adapter
    private var hasNextPage = true
    private var previousTotalItemCount = 0
    val scrollListener: RecyclerView.OnScrollListener
    val dataObserver: RecyclerView.AdapterDataObserver

    init {
        dataObserver = object : RecyclerView.AdapterDataObserver() {
            override fun onChanged() {
                hasNextPage = true
                previousTotalItemCount = 0
            }

            override fun onItemRangeInserted(positionStart: Int, itemCount: Int) {
                if (positionStart == previousTotalItemCount && itemCount == pageSize) {
                    hasNextPage = true
                }
            }

            override fun onItemRangeChanged(positionStart: Int, itemCount: Int) {
                if (positionStart == previousTotalItemCount && itemCount == pageSize) {
                    hasNextPage = true
                }
            }
        }
        adapter?.registerAdapterDataObserver(dataObserver)
        scrollListener = object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0) {
                    val lastVisibleItemPosition = layoutManager.findLastVisibleItemPosition()
                    val totalItemCount = layoutManager.itemCount
                    if (hasNextPage && lastVisibleItemPosition + visibleThreshold > totalItemCount) {
                        previousTotalItemCount = totalItemCount
                        hasNextPage = false
                        offsetListener(previousTotalItemCount)
                    }
                }
            }
        }
        recyclerView.addOnScrollListener(scrollListener)
    }
}