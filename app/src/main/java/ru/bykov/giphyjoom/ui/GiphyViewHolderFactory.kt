package ru.bykov.giphyjoom.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import ru.bykov.giphyjoom.R

/**
 * @author Grigorii Bykov
 */
interface GiphyViewHolderFactory {

    fun createHolder(parent: ViewGroup, viewType: Int): GiphyViewHolder<GiphyDisplaybleItem>
}

class GiphyViewHolderFactoryImpl(
    context: Context,
    private val clickListener: (Int) -> Unit,
    private val errorClickListener: () -> Unit
) : GiphyViewHolderFactory {

    private val inflater: LayoutInflater = LayoutInflater.from(context)

    private val options: RequestOptions = RequestOptions()
        .format(DecodeFormat.PREFER_RGB_565)
        .centerCrop()
        .sizeMultiplier(0.5F)
        .diskCacheStrategy(DiskCacheStrategy.RESOURCE)

    override fun createHolder(parent: ViewGroup, viewType: Int): GiphyViewHolder<GiphyDisplaybleItem> {
        val view = inflater.inflate(viewType, parent, false)
        val viewholder = when (viewType) {
            R.layout.item_offset_loading -> OffsetLoadingViewHolder(view)
            R.layout.item_offset_error -> ErrorLoadingViewHolder(view, errorClickListener)
            else -> GiphyImageViewHolder(view, options, clickListener)
        }
        return viewholder as GiphyViewHolder<GiphyDisplaybleItem>
    }

}