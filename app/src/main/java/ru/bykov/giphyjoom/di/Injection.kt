package ru.bykov.giphyjoom.di

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import ru.bykov.giphyjoom.model.GiphyApi
import ru.bykov.giphyjoom.model.GiphyImagesRepository
import ru.bykov.giphyjoom.model.InMemoryGiphyImagesRepository

/**
 * @author Grigorii Bykov
 */
object Injection {

    private val httpClient: OkHttpClient by lazy {
        OkHttpClient.Builder()
            .addInterceptor(apiKeyRequestInterceptor())
            .build()
    }

    private val gson: Gson by lazy {
        GsonBuilder().create()
    }

    private val retrofit: Retrofit by lazy {
        Retrofit.Builder()
            .addCallAdapterFactory(
                RxJava2CallAdapterFactory.create()
            )
            .addConverterFactory(
                GsonConverterFactory.create(gson)
            )
            .client(httpClient)
            .baseUrl("https://api.giphy.com/")
            .build()
    }

    private val giphyApi: GiphyApi by lazy {
        retrofit.create(GiphyApi::class.java)
    }

    val repository: GiphyImagesRepository by lazy {
        InMemoryGiphyImagesRepository(giphyApi)
    }

    private fun apiKeyRequestInterceptor(): Interceptor {
        return Interceptor { chain ->
            val original = chain.request()
            val originalHttpUrl = original.url()

            val url = originalHttpUrl.newBuilder()
                .addQueryParameter("api_key", "HHzzbgnTqgLE1X4lMUPuA4iFHXjG9Va8")
                .build()

            val requestBuilder = original.newBuilder()
                .url(url)

            val request = requestBuilder.build()
            return@Interceptor chain.proceed(request)
        }
    }
}