package ru.bykov.giphyjoom.di

import ru.bykov.giphyjoom.GiphyImagesAdapter
import ru.bykov.giphyjoom.MainActivity
import ru.bykov.giphyjoom.model.GiphyImagesRepository
import ru.bykov.giphyjoom.ui.GiphyDisplaybleItem
import ru.bykov.giphyjoom.ui.GiphyImageItem
import ru.bykov.giphyjoom.ui.GiphyViewHolderFactory
import ru.bykov.giphyjoom.ui.GiphyViewHolderFactoryImpl
import ru.bykov.giphyjoom.ui.OnImageClickListener
import ru.bykov.giphyjoom.ui.OnRetryClickListener

/**
 * @author Grigorii Bykov
 */
class ListInjection(
    activity: MainActivity,
    onImageClickListener: OnImageClickListener,
    onRetryClickListener: OnRetryClickListener
) {

    private val items: MutableList<GiphyDisplaybleItem> = mutableListOf()

    private val holderFactory: GiphyViewHolderFactory by lazy {
        GiphyViewHolderFactoryImpl(
            context = activity,
            clickListener = {
                val giphyImageItem = items[it] as GiphyImageItem
                onImageClickListener.onImageClicked(giphyImageItem.giphyImage)
            },
            errorClickListener = {
                onRetryClickListener.onRetryClicked(items.size - 1)
            }
        )
    }

    val adapter: GiphyImagesAdapter by lazy {
        GiphyImagesAdapter(items, holderFactory)
    }

    val repository: GiphyImagesRepository = Injection.repository
}