package ru.bykov.giphyjoom

import ru.bykov.giphyjoom.model.GiphyImageDetails

/**
 * @author Grigorii Bykov
 */
object DetailsContract {

    interface View {

        fun showDetails(details: GiphyImageDetails)

        fun showError()
    }

    interface Presenter {

        fun loadImageDetails(id: String)

        fun destroy()
    }
}