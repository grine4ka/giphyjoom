package ru.bykov.giphyjoom.model

/**
 * @author Grigorii Bykov
 */
class MyHttpException(
    val code: Int,
    responseMessage: String
) : RuntimeException("HTTP $code $responseMessage")