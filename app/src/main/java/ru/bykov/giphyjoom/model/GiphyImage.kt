package ru.bykov.giphyjoom.model

/**
 * @author Grigorii Bykov
 */
class GiphyImage(
    val id: String,
    val url: String
)