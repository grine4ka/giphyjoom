package ru.bykov.giphyjoom.model

import io.reactivex.Completable
import io.reactivex.Single
import ru.bykov.giphyjoom.extensions.switchToSingleIfEmpty
import ru.bykov.giphyjoom.extensions.toMaybe

/**
 * @author Grigorii Bykov
 */
interface GiphyImagesRepository {

    fun clear(): Completable

    fun images(offset: Int = 0): Single<List<GiphyImage>>

    fun details(id: String): Single<GiphyImageDetails>
}

class InMemoryGiphyImagesRepository(
    private val giphyApi: GiphyApi
) : GiphyImagesRepository {

    private val images: MutableMap<String, GiphyApiObjectItem> = mutableMapOf()

    override fun clear(): Completable {
        return Completable.fromAction { images.clear() }
    }

    override fun images(offset: Int): Single<List<GiphyImage>> {
        return giphyApi.getTrendingImages(Payload.LIMIT, offset)
            .map { payload ->
                if (payload.meta.status == Meta.STATUS_OK) {
                    val data = payload.data
                    data.forEach { image -> images[image.id] = image }
                    data.map { item -> GiphyImage(item.id, item.loadingUrl()) }
                } else {
                    throw MyHttpException(payload.meta.status, payload.meta.msg)
                }
            }
    }

    override fun details(id: String): Single<GiphyImageDetails> {
        return images[id].toMaybe()
            .switchToSingleIfEmpty {
                giphyApi.getById(id)
                    .map { payload ->
                        if (payload.meta.status == Meta.STATUS_OK) {
                            val image = payload.data
                            images[image.id] = image
                            image
                        } else {
                            throw MyHttpException(payload.meta.status, payload.meta.msg)
                        }
                    }
            }
            .map { image ->
                val user = image.user
                GiphyImageDetails(
                    image.loadingUrl(),
                    user?.userName,
                    user?.displayName,
                    user?.profileUrl,
                    user?.twitter
                )
            }
    }
}