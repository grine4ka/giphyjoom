package ru.bykov.giphyjoom.model

/**
 * @author Grigorii Bykov
 */
class GiphyImageDetails(
    val url: String,
    val userName: String?,
    val realName: String?,
    val profileLink: String?,
    val twitterName: String?
)