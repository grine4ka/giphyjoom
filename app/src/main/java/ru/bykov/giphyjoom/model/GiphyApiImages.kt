package ru.bykov.giphyjoom.model

import com.google.gson.annotations.SerializedName

/**
 * @author Grigorii Bykov
 */
class GiphyApiImages(
    @SerializedName("original_still")
    val imageStill: GiphyApiImage
)