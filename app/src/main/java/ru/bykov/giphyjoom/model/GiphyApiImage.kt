package ru.bykov.giphyjoom.model

import com.google.gson.annotations.SerializedName

/**
 * @author Grigorii Bykov
 */
class GiphyApiImage(
    @SerializedName("url")
    val url: String
)