package ru.bykov.giphyjoom.model

import com.google.gson.annotations.SerializedName

/**
 * @author Grigorii Bykov
 */
class GiphyApiObjectItem(
    @SerializedName("id")
    val id: String,
    @SerializedName("user")
    val user: GiphyApiUser?,
    @SerializedName("images")
    private val images: GiphyApiImages
) {

    fun loadingUrl(): String {
        return images.imageStill.url
    }
}