package ru.bykov.giphyjoom.model

import com.google.gson.annotations.SerializedName

/**
 * @author Grigorii Bykov
 */
class Pagination(
    @SerializedName("offset")
    private val offset: Int,
    @SerializedName("total_count")
    private val totalCount: Int,
    @SerializedName("count")
    private val count: Int
)