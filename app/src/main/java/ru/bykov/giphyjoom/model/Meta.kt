package ru.bykov.giphyjoom.model

import com.google.gson.annotations.SerializedName

/**
 * @author Grigorii Bykov
 */
class Meta(
    @SerializedName("msg")
    val msg: String,
    @SerializedName("status")
    val status: Int
) {

    companion object {
        const val STATUS_OK = 200
    }
}