package ru.bykov.giphyjoom.model

import com.google.gson.annotations.SerializedName

/**
 * @author Grigorii Bykov
 */
class GiphyApiUser(
    @SerializedName("profile_url")
    val profileUrl: String?,
    @SerializedName("username")
    val userName: String?,
    @SerializedName("display_name")
    val displayName: String?,
    @SerializedName("twitter")
    val twitter: String?
)