package ru.bykov.giphyjoom.model

import com.google.gson.annotations.SerializedName

/**
 * @author Grigorii Bykov
 */
class Payload<T>(
    @SerializedName("data")
    val data: T,
    @SerializedName("pagination")
    val pagination: Pagination?,
    @SerializedName("meta")
    val meta: Meta
) {
    companion object {
        const val LIMIT = 50
    }
}