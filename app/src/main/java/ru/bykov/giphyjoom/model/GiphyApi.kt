package ru.bykov.giphyjoom.model

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * @author Grigorii Bykov
 */
interface GiphyApi {

    @GET("v1/gifs/trending")
    fun getTrendingImages(
        @Query("limit") limit: Int,
        @Query("offset") offset: Int
    ): Single<Payload<List<GiphyApiObjectItem>>>

    @GET("/v1/gifs/{id}")
    fun getById(@Path("id") id: String): Single<Payload<GiphyApiObjectItem>>
}