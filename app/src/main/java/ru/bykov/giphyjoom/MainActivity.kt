package ru.bykov.giphyjoom

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.main_activity.*
import ru.bykov.giphyjoom.di.ListInjection
import ru.bykov.giphyjoom.extensions.toast
import ru.bykov.giphyjoom.model.GiphyImage
import ru.bykov.giphyjoom.ui.GiphyImageItem
import ru.bykov.giphyjoom.ui.GiphyViewHolder
import ru.bykov.giphyjoom.ui.OnImageClickListener
import ru.bykov.giphyjoom.ui.OnRetryClickListener
import ru.bykov.giphyjoom.ui.offsetListener

class MainActivity : AppCompatActivity(), MainContract.View, OnImageClickListener,
    OnRetryClickListener {

    private val injection: ListInjection by lazy {
        ListInjection(this, this, this)
    }

    private val adapter: GiphyImagesAdapter by lazy {
        injection.adapter
    }

    private lateinit var presenter: MainContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)

        imagesRecyclerView.adapter = adapter
        imagesRecyclerView.setRecyclerListener {
            val viewHolder = it as GiphyViewHolder<*>
            viewHolder.recycle()
        }
        imagesRecyclerView.offsetListener { offset ->
            adapter.onOffsetStartLoad()
            presenter.loadImagesWithOffset(offset)
        }

        swipeRefreshLayout.setOnRefreshListener {
            presenter.loadImagesFromStart()
        }

        presenter = MainPresenter(injection.repository, this)
        presenter.loadImagesFromStart()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.destroy()
    }

    override fun showImages(images: List<GiphyImageItem>) {
        progressBar.visibility = View.GONE
        imagesRecyclerView.visibility = View.VISIBLE
        swipeRefreshLayout.isRefreshing = false
        adapter.setItems(images)
    }

    override fun addImages(
        offset: Int,
        images: List<GiphyImageItem>
    ) {
        adapter.addItems(offset, images)
    }

    override fun showError() {
        progressBar.visibility = View.GONE
        toast(getString(R.string.default_error))
    }

    override fun showOffsetLoadingError() {
        adapter.onOffsetLoadError()
    }

    override fun onImageClicked(image: GiphyImage) {
        showGiphyDetails(image.id)
    }

    override fun onRetryClicked(offset: Int) {
        adapter.onOffsetStartLoad()
        presenter.loadImagesWithOffset(offset)
    }
}
