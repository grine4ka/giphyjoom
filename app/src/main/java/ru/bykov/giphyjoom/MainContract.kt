package ru.bykov.giphyjoom

import ru.bykov.giphyjoom.ui.GiphyImageItem

/**
 * @author Grigorii Bykov
 */
object MainContract {

    interface View {

        fun showImages(images: List<GiphyImageItem>)

        fun addImages(
            offset: Int,
            images: List<GiphyImageItem>
        )

        fun showError()

        fun showOffsetLoadingError()
    }

    interface Presenter {

        fun loadImagesFromStart()

        fun loadImagesWithOffset(offset: Int)

        fun destroy()
    }
}