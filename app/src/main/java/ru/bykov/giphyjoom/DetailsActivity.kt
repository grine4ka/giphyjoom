package ru.bykov.giphyjoom

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_gif_details.*
import ru.bykov.giphyjoom.di.Injection
import ru.bykov.giphyjoom.extensions.show
import ru.bykov.giphyjoom.extensions.toast
import ru.bykov.giphyjoom.model.GiphyImageDetails

private const val EXTRA_GIF_ID = "extra_gif_id"

fun Activity.showGiphyDetails(gifId: String) {
    startActivity(
        Intent(this, DetailsActivity::class.java).apply {
            putExtra(EXTRA_GIF_ID, gifId)
        }
    )
}

class DetailsActivity : AppCompatActivity(), DetailsContract.View {

    private lateinit var presenter: DetailsContract.Presenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gif_details)

        presenter = DetailsPresenter(Injection.repository, this)
        presenter.loadImageDetails(extractGifId(intent))
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.destroy()
    }

    override fun showDetails(details: GiphyImageDetails) {
        Glide.with(this)
            .load(details.url)
            .into(image)
        details.userName?.let {
            userName.text = details.userName
        }
        details.realName?.let {
            realNameLabel.show()
            realName.show()
            realName.text = it
        }
        details.twitterName?.let {
            twitterNameLabel.show()
            twitterHandle.show()
            twitterHandle.text = it
        }
        details.profileLink?.let {
            profileLinkLabel.show()
            profile.show()
            profile.text = it
        }
    }

    override fun showError() {
        toast(getString(R.string.default_error))
    }

    private fun extractGifId(intent: Intent): String {
        val data = intent.data
        return if (Intent.ACTION_VIEW == intent.action && data != null) {
            data.lastPathSegment!!
        } else {
            intent.getStringExtra(EXTRA_GIF_ID)
        }
    }
}
