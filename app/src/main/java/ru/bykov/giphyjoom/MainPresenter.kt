package ru.bykov.giphyjoom

import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import ru.bykov.giphyjoom.extensions.async
import ru.bykov.giphyjoom.model.GiphyImagesRepository
import ru.bykov.giphyjoom.ui.GiphyImageItem

/**
 * @author Grigorii Bykov
 */
class MainPresenter(
    private val imagesRepository: GiphyImagesRepository,
    private val view: MainContract.View,
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
) : MainContract.Presenter {

    override fun loadImagesFromStart() {
        compositeDisposable.add(
            loadImagesSingle()
                .subscribe(
                    { view.showImages(it) },
                    { view.showError() }
                )
        )
    }

    override fun loadImagesWithOffset(offset: Int) {
        compositeDisposable.add(
            loadImagesSingle(offset)
                .subscribe(
                    { view.addImages(offset, it) },
                    {
                        view.showOffsetLoadingError()
                        view.showError()
                    }
                )
        )
    }

    private fun loadImagesSingle(offset: Int = 0): Single<List<GiphyImageItem>> {
        return imagesRepository.images(offset)
            .map { images -> images.map { GiphyImageItem(it) } }
            .async()
    }

    override fun destroy() {
        compositeDisposable.clear()
    }
}