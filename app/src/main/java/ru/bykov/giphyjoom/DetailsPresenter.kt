package ru.bykov.giphyjoom

import io.reactivex.disposables.CompositeDisposable
import ru.bykov.giphyjoom.extensions.async
import ru.bykov.giphyjoom.model.GiphyImagesRepository

/**
 * @author Grigorii Bykov
 */
class DetailsPresenter(
    private val imagesRepository: GiphyImagesRepository,
    private val view: DetailsContract.View,
    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
) : DetailsContract.Presenter {

    override fun loadImageDetails(id: String) {
        compositeDisposable.add(
            imagesRepository.details(id)
                .async()
                .subscribe(
                    { view.showDetails(it) },
                    { view.showError() }
                )
        )
    }

    override fun destroy() {
        compositeDisposable.clear()
    }
}