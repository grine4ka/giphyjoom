package ru.bykov.giphyjoom

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ru.bykov.giphyjoom.ui.ErrorLoadingItem
import ru.bykov.giphyjoom.ui.GiphyDisplaybleItem
import ru.bykov.giphyjoom.ui.GiphyViewHolder
import ru.bykov.giphyjoom.ui.GiphyViewHolderFactory
import ru.bykov.giphyjoom.ui.OffsetLoadingItem

/**
 * @author Grigorii Bykov
 */
class GiphyImagesAdapter(
    private val items: MutableList<GiphyDisplaybleItem> = mutableListOf(),
    private val holderFactory: GiphyViewHolderFactory
) : RecyclerView.Adapter<GiphyViewHolder<GiphyDisplaybleItem>>() {

    override fun getItemCount(): Int = items.size

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): GiphyViewHolder<GiphyDisplaybleItem> {
        return holderFactory.createHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: GiphyViewHolder<GiphyDisplaybleItem>, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemViewType(position: Int): Int = items[position].viewType

    fun setItems(newItems: List<GiphyDisplaybleItem>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun addItems(offset: Int, newItems: List<GiphyDisplaybleItem>) {
        val index = lastItemIndex()
        items.removeAt(index)
        notifyItemRemoved(index)
        items.addAll(offset, newItems)
        notifyItemRangeInserted(offset, newItems.size)
    }

    fun onOffsetLoadError() {
        items.remove(OffsetLoadingItem)
        items.add(ErrorLoadingItem)
        notifyItemChanged(items.size)
    }

    fun onOffsetStartLoad() {
        items.remove(ErrorLoadingItem)
        items.add(OffsetLoadingItem)
        notifyItemChanged(items.size)
    }

    private fun lastItemIndex(): Int = items.size - 1
}