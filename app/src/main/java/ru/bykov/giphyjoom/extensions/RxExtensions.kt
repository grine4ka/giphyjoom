package ru.bykov.giphyjoom.extensions

import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * @author Grigorii Bykov
 */
fun <T> Single<T>.async(): Single<T> {
    return subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
}

fun <T> T?.toMaybe(): Maybe<T> {
    return if (this == null) {
        Maybe.empty()
    } else {
        Maybe.just(this)
    }
}

inline fun <T> Maybe<T>.switchToSingleIfEmpty(other: () -> Single<T>): Single<T> {
    return switchIfEmpty(other())
}