package ru.bykov.giphyjoom.extensions

import android.content.Context
import android.view.View
import android.widget.Toast

/**
 * @author Grigorii Bykov
 */
fun Context.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
}

fun View.show() {
    visibility = View.VISIBLE
}
